# Fotoclub Ontspanner Lebbeke -- Website backend

Currently consists of a Python 2.x script that fetches our Google Calendar through the Google calendar API and exports
it as JSON. This script is designed to run as a cron job.

Development is done using `virtualenv`. All dependencies are listed in `requirements.pip`.


## Setting up

The virtual environment can be created using:

```sh
virtualenv-2 env
source env/bin/activate
pip install -r requirements.pip
```

It can later be reused with the following command:

```sh
source env/bin/activate
```


## Maintenance

When packages are upgraded in the virtual environment, the `requirements.pip` file must be updated by running the
following command from within the virtual environment:

```sh
pip freeze > requirements.pip
```


## Deployment

The script is configured through the configuration file `calendar.conf`. This file can be stored in the same directory
as the script for testing purposes. Upon deployment, it should be stored in `/etc/ontspanner`. The directory to which
the script itself is deployed does not matter and does not need to be configured anywhere. The generated JSON data is
stored in the configured `DATA_DIR`.

Installation is performed using traditional `make`, e.g.:

```sh
make install DESTDIR=/usr
```
