.PHONY: all install

all:

install: src/export_calendar.py src/export_calendar.sh
	$(if $(value DESTDIR),,$(error you need to define the destination directory through the DESTDIR variable))
	pip2 install -r requirements.pip
	install -d $(DESTDIR)/bin
	install -m 0555 $^ $(DESTDIR)/bin
