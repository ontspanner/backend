#!/usr/bin/env python2

from __future__ import absolute_import, division, print_function, unicode_literals

import apiclient
import argparse
import datetime
import httplib2
import iso8601utils.parsers
import iso8601utils.validators
import json
import os
import pprint
import sys


ZERO = datetime.timedelta(0)

# How soon modifications are exported; if more recent modifications are detected the script will terminate; skipping
# recently modified events is not an option as it may suddenly remove events already exported in the past. The proper
# way is to track the history of an event---too complex for our use case.
UPDATE_DELAY = datetime.timedelta(minutes=2)


class RecentlyModifiedException(Exception):
    pass


class UTC(datetime.tzinfo):

    def utcoffset(self, dt):
        return ZERO

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return ZERO


def process_event(request_datetime_utc, event):

    # All events should have an HTML link
    if 'htmlLink' not in event:
        raise ValueError('event without HTML link')

    # Only handle 'confirmed' events
    if event.get('status') != 'confirmed':
        print('Skipping unconfirmed event:', event['htmlLink'], file=sys.stderr)
        return None

    # Skip events that were updated in the last 5 minutes
    if 'updated' not in event:
        raise ValueError('event without update timestamp')
    try:
        updated = iso8601utils.parsers.datetime(event['updated'])
    except:
        raise ValueError('event with invalid update timestamp "{0}"'.format(event['updated']))
    age = request_datetime_utc - updated
    if (age < UPDATE_DELAY):
        print('Found event that has been updated only {0} ago: {1}'.format(age, event['htmlLink']), file=sys.stderr)
        raise RecentlyModifiedException()

    # Make sure all other required fields are present
    if (not isinstance(event.get('summary'), basestring)) or (not event['summary'].strip()):
        raise ValueError('event without summary')
    if (not type(event.get('start')) is dict):
        raise ValueError('event without start')
    if (not type(event.get('end')) is dict):
        raise ValueError('event without end')

    # Verify start / end timestamps; skip all-day events and events with custom timezones
    if 'dateTime' not in event['start']:
        if 'date' in event['start']:
            print('Skipping all-day event:', event['htmlLink'], file=sys.stderr)
            return None
        raise ValueError('event with invalid start')
    if 'timeZone' in event['start']:
        raise ValueError('event with custom start timezone')
    if 'dateTime' not in event['end']:
        raise ValueError('event with invalid end')
    if 'timeZone' in event['end']:
        raise ValueError('event with custom end timezone')
    if not iso8601utils.validators.datetime(event['start']['dateTime']):
        raise ValueError('event with invalid start timestamp')
    if not iso8601utils.validators.datetime(event['end']['dateTime']):
        raise ValueError('event with invalid end timestamp')

    # Create object for export with all mandatory fields
    export_event = {
        'title': event['summary'].strip(),
        'start': event['start']['dateTime'],
        'end': event['end']['dateTime'],
        'url': event['htmlLink']
    }

    # Add optional location field
    location = event.get('location')
    if isinstance(location, basestring):
        location = location.strip()
        if location:
            export_event['location'] = location

    # Add optional summary field
    description = event.get('description')
    if isinstance(description, basestring):
        description = description.strip()
        if description:
            export_event['description'] = description

    return export_event


def process_events(request_datetime_utc, events):

    # Create pretty printer to dump unhandled events
    pretty_printer = pprint.PrettyPrinter(indent=4, stream=sys.stderr)

    # Process events one by one
    export_events = []
    for event in events:

        # Process event; pretty-print on errors
        try:
            export_event = process_event(request_datetime_utc, event)
        except ValueError as error:
            print(error, file=sys.stderr)
            pretty_printer.pprint(event)
            continue

        # Some events may be skipped without the need to dump them (unconfirmed and too recently updated events)
        if export_event is not None:
            export_events.append(export_event)

    return export_events


def main():

    # Do not run as root
    if os.geteuid() == 0:
        print('Fatal: this script must not be run as root', file=sys.stderr)
        return 1

    # Parse command line arguments
    parser = argparse.ArgumentParser(description='Export Google calendar events to JSON.')
    parser.add_argument('-c', dest='calendar', metavar='CALENDAR', type=str, required=True, help='calendar name')
    parser.add_argument('-H', dest='debug_http', action='store_true', help='debug HTTP requests')
    parser.add_argument('-k', dest='api_key', metavar='API_KEY', type=str, required=True, help='API key')
    parser.add_argument('-o', dest='output_file', metavar='OUTPUT_FILE', type=str, required=True, help='output file name')
    arguments = parser.parse_args()

    # Configure HTTP debugging in httplib2, which is used by the google API client library
    if arguments.debug_http:
        httplib2.debuglevel = 4

    # Build a request to retrieve upcoming events
    service = apiclient.discovery.build('calendar', 'v3', developerKey=arguments.api_key)
    calendar_request_datetime_utc = datetime.datetime.now(UTC())
    calendar_request_datetime_utc_string = calendar_request_datetime_utc.isoformat()
    calendar_request = service.events().list(calendarId=arguments.calendar,
                                             fields='items(description,end,htmlLink,location,start,status,summary,updated)',
                                             maxResults=24, orderBy='startTime', singleEvents=True,
                                             timeMin=calendar_request_datetime_utc_string)

    # Execute the request
    try:
        events = calendar_request.execute().get('items', [])
    except apiclient.errors.HttpError as http_error:
        print('Fatal: HTTP request failed ({0})'.format(http_error.resp.status), file=sys.stderr)
        if http_error.resp.status in [400, 404]:
            if http_error.resp.get('content-type', '').startswith('application/json'):
                errors = json.loads(http_error.content).get('error')
                if errors is not None:
                    for error in errors.get('errors'):
                        domain = error.get('domain')
                        reason = error.get('reason')
                        if domain == 'global' and reason == 'notFound':
                            print('Reason: calendar not found', file=sys.stderr)
                        elif domain == 'usageLimits' and reason == 'keyInvalid':
                            print('Reason: invalid API key', file=sys.stderr)
                        else:
                            print('Reason: unknown; domain="{0}"; reason="{1}"'.format(domain, reason), file=sys.stderr)
        return 1

    try:
        json_output = json.dumps(process_events(calendar_request_datetime_utc, events))
    except RecentlyModifiedException:
        return 2

    output_file_temp_name = arguments.output_file
    try:
        output_file = open(output_file_temp_name, 'w')
        output_file.write(json_output)
        output_file.close()
    except IOError as error:
        print('Fatal: failed to write temporary output file: {0} ({1})'.format(error.strerror, error.errno), file=sys.stderr)
        return 3

    try:
        os.rename(output_file_temp_name, arguments.output_file)
    except OSError as error:
        try:
            os.remove(output_file_temp_name)
        except:
            pass
        print('Fatal: failed to write output file: {0} ({1})'.format(error.strerror, error.errno), file=sys.stderr)
        return 4

    return 0


if __name__ == '__main__':
    sys.exit(main())
