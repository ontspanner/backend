#!/bin/sh

APP_DIR="`realpath "$0"`"
APP_DIR="`dirname "${APP_DIR}"`"
CONFIG_ETC_DIR='/etc/ontspanner'
CONFIG_FILENAME='calendar.conf'

exit_fatal() {
  echo "$0: fatal: $1" >&2
  exit 1
}

[[ `id -u` != 0 ]] || exit_fatal 'this script must not be run as root'

if [[ -f "${APP_DIR}/${CONFIG_FILENAME}" ]]; then
  source "${APP_DIR}/${CONFIG_FILENAME}"
elif [[ -f "${CONFIG_ETC_DIR}/${CONFIG_FILENAME}" ]]; then
  source "${CONFIG_ETC_DIR}/${CONFIG_FILENAME}"
fi

[[ -n "${DATA_DIR}" ]]        || exit_fatal 'data directory must be set in environment variable DATA_DIR'
[[ -n "${GOOGLE_API_KEY}" ]]  || exit_fatal 'Google API key must be set in environment variable GOOGLE_API_KEY'
[[ -n "${GOOGLE_CALENDAR}" ]] || exit_fatal 'Google calendar must be set in environment variable GOOGLE_CALENDAR'

python2 "${APP_DIR}/export_calendar.py" -c "${GOOGLE_CALENDAR}" -k "${GOOGLE_API_KEY}" -o "${DATA_DIR}/events.json"
